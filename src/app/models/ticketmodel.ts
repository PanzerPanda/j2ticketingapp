export class Ticket {
    constructor(
        public ticketNo: string,
        public afe: string,
        public customer: string,
        public discount: number,
        public well: string,
        public county: string,
        public state: string,
        public date: string,
        public seller: string,
        public trailer: string,
        public truck: string,
        public tax: number,
        public directions: string,
        public details: string,
        public transporttime: number,
        public transportdistance: number,
        public orders: Order[],
        public additives: Additive[],
        public completed: boolean,
        public address: string,
        public fieldrep: string,
        public specialdirections: string,
        public stamp: string,
        public signature: string,
        public invoice: boolean,
        public email: string
    ) {}
}

export class Order {
    constructor(
        public cost: number,
        public perUnit: number,
        public item: string,
        public quantity: number,
        public type: string,
        public cog: boolean,
        public perThousand?: number
    ) {}
}

export class Additive {
    constructor(
        public cost: number,
        public perUnit: number,
        public item: string,
        public quantity: string,
    ) {}
}
