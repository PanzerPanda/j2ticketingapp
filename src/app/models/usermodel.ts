export interface User {
    uid: string;
    email: string;
    displayName?: string;
    role: string;
    fullEmail?: boolean;
    password: string;
}
