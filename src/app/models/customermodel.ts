export interface Customer {
    name: string;
    seller: string;
    discount: number;
    tax: number;
    state: string;
    id?: string;
}
