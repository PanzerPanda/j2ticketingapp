export interface County {
    name: string;
    tax: number;
    state: string;
}
