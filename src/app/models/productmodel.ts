export interface Product {
    code: string;
    providercode: string;
    provider: string;
    description: string;
    details: string;
    costperunit: number;
    unit: string;
    priceperunit: number;
    retailprice: number;
    markup: number;
    multiplier: number;
    type: string;
    cog: boolean;
    perThousand?: number;
}
