export class FieldRep {
    constructor(
        public name: string,
        public email: string,
        public customer: string
    ) {}
}
