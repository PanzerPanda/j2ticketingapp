export class Stamp {
    constructor(
        public customer: string,
        public stampBase64: string,
        public well: string
        ) {}
}
