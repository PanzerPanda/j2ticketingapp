import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { User } from 'src/app/models/usermodel';
import { ToastController } from '@ionic/angular';
import { DblinkService } from './dblink.service';
import * as firebase from 'firebase';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  user$: Observable<User>;

  constructor(private afAuth: AngularFireAuth, private db: DblinkService, private router: Router, private toastCtrl: ToastController) {
    this.afAuth.setPersistence(firebase.auth.Auth.Persistence.LOCAL); //breaks the app due to angular/fire v6 removing auth
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.db.userCollection.doc<User>(`${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }

  async signIn(id: string, password: string) {
    await this.afAuth.signInWithEmailAndPassword(id + '@gmail.com', password)
    .then(
      resData => {
        console.log('logged in', resData);
        this.router.navigate(['/', 'home']);
      },
      err => {
        console.log ('login failed', err);
      }
    );
  }

  async registerRealEmail(id: string, password: string, displayName: string, role: string) {
    console.log('id', id);
    console.log('password', password);
    await this.afAuth.createUserWithEmailAndPassword(id, password)
    .then(
      resData => {
        console.log('confirmation', resData);
        return this.updateUserData(resData.user, displayName, role, password);
      },
      err => {
        console.log('error', err);
        if (err.code === 'auth/email-already-in-use') {
          this.showErrorToast(err.message);
        }
        if (err.code === 'auth/weak-password') {
          this.showErrorToast(err.message);
        }
        if (err.code === 'auth/invalid-email') {
          this.showErrorToast(err.message);
        }
      }
    );
  }

  async registerPseudoEmail(id: string, password: string, displayName: string, role: string) {
    console.log('id', id);
    console.log('password', password);
    await this.afAuth.createUserWithEmailAndPassword(id + '@gmail.com', password)
    .then(
      resData => {
        console.log('confirmation', resData);
        return this.updateUserData(resData.user, displayName, role, password);
      },
      err => {
        console.log('error', err);
        if (err.code === 'auth/email-already-in-use') {
          this.showErrorToast(err.message);
        }
        if (err.code === 'auth/weak-password') {
          this.showErrorToast(err.message);
        }
        if (err.code === 'auth/invalid-email') {
          this.showErrorToast(err.message);
        }
      }
    );
  }

  async logout() {
    await this.afAuth.signOut();
    this.router.navigate(['/login']);
  }

  updateUserData(user, displayName, role, password) {
    const userRef: AngularFirestoreDocument<User> = this.db.userCollection.doc(`${user.uid}`);
    const data = {
      uid: user.uid,
      email: user.email,
      displayName,
      role,
      password
    };
    userRef.set(data, {merge: true});
    this.router.navigate(['/home']);
  }

  showErrorToast(data: any) {
    this.toastCtrl.create({
      message: data,
      duration: 2000
    }).then(toaster => {
      toaster.present();
    });
  }
}
