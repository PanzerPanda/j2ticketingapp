import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Customer } from '../models/customermodel';
import { Product } from '../models/productmodel';
import { Ticket } from '../models/ticketmodel';
import { County } from '../models/countymodel';
import { Stamp } from '../models/stampmodel';
import { FieldRep } from '../models/fieldrepmodel';
import { User } from '../models/usermodel';
import { Vehicle } from '../models/vehiclemodel';

@Injectable({
  providedIn: 'root'
})
export class DblinkService {

  completeticketCollection: AngularFirestoreCollection<Ticket>;
  uncompleteticketCollection: AngularFirestoreCollection<Ticket>;
  customerCollection: AngularFirestoreCollection<Customer>;
  productsCollection: AngularFirestoreCollection<Product>;
  allProductsCollection: AngularFirestoreCollection<Product>;
  ticketCollection: AngularFirestoreCollection<Ticket>;
  countyCollection: AngularFirestoreCollection<County>;
  stampCollection: AngularFirestoreCollection<Stamp>;
  trailerCollection: AngularFirestoreCollection<Vehicle>;
  truckCollection: AngularFirestoreCollection<Vehicle>;
  vehicleCollection: AngularFirestoreCollection<Vehicle>;
  buyerCollection: AngularFirestoreCollection;
  fieldRepCollection: AngularFirestoreCollection;
  customerFieldRepCollection: AngularFirestoreCollection<FieldRep>;
  customerBuyerCollection: AngularFirestoreCollection<FieldRep>;
  userCollection: AngularFirestoreCollection<User>;

  constructor(private afs: AngularFirestore) {
    this.completeticketCollection = this.afs.collection('tickets', ref => {
      return ref.where('completed', '==', true).orderBy('ticketNo', 'desc');
    });
    this.uncompleteticketCollection = this.afs.collection('tickets', ref => {
      return ref.where('completed', '==', false).orderBy('ticketNo', 'desc');
    });
    this.customerCollection = this.afs.collection<Customer>('customers');
    this.productsCollection = this.afs.collection<Product>('products', ref => {
      return ref.where('cog', '==', false);
    });
    this.allProductsCollection = this.afs.collection<Product>('products');
    this.countyCollection = this.afs.collection<County>('counties');
    this.stampCollection = this.afs.collection<Stamp>('stamps');
    this.ticketCollection = this.afs.collection<Ticket>('tickets', ref => {
      return ref.orderBy('ticketNo', 'desc');
    });
    this.trailerCollection = this.afs.collection<Vehicle>('vehicles', ref => {
      return ref.where('type', '==', 'trailer');
    });
    this.truckCollection = this.afs.collection<Vehicle>('vehicles', ref => {
      return ref.where('type', 'in', ['truck', 'killtruck', 'datavan']);
    });
    this.vehicleCollection = this.afs.collection<Vehicle>('vehicles');
    this.buyerCollection = this.afs.collection<FieldRep>('fieldrep');
    this.fieldRepCollection = this.afs.collection<FieldRep>('fieldrep');
    this.userCollection = this.afs.collection<User>('users');
  }

  getCustomerInfo(customerID: string): AngularFirestoreDocument<Customer> {
    return this.customerCollection.doc(customerID);
  }

  getProductInfo(productID: string): AngularFirestoreDocument<Product> {
    return this.productsCollection.doc(productID);
  }

  getVehicleInfo(vehicleID: string): AngularFirestoreDocument<Vehicle> {
    return this.vehicleCollection.doc(vehicleID);
  }

  getTicketInfo(ticketNo: string): AngularFirestoreDocument<Ticket> {
    return this.ticketCollection.doc(ticketNo);
  }

  getStampInfo(stampWell: string): AngularFirestoreDocument<Stamp> {
    return this.stampCollection.doc(stampWell);
  }

  getCountyInfo(county: string): AngularFirestoreDocument<County> {
    return this.countyCollection.doc(county);
  }

  getCustomerBuyers(customer: string) {
    this.customerBuyerCollection = this.afs.collection('fieldrep', ref => {
      return ref.where('customer', '==', customer);
    });
  }

  getBuyerInfo(buyer: string): AngularFirestoreDocument<FieldRep> {
    return this.buyerCollection.doc(buyer);
  }

  getCustomerFieldReps(customer: string) {
    this.customerFieldRepCollection = this.afs.collection('fieldrep', ref => {
      return ref.where('customer', '==', customer);
    });
  }

  getFieldRepInfo(fieldRep: string): AngularFirestoreDocument<FieldRep> {
    return this.fieldRepCollection.doc(fieldRep);
  }

  deleteTicket(ticketNo: string) {
    this.ticketCollection.doc(ticketNo).delete();
  }

  base64toBlob(base64Data, contentType) {
    contentType = contentType || '';
    const sliceSize = 1024;
    const byteCharacters = atob(base64Data);
    const bytesLength = byteCharacters.length;
    const slicesCount = Math.ceil(bytesLength / sliceSize);
    const byteArrays = new Array(slicesCount);

    for (let sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
      const begin = sliceIndex * sliceSize;
      const end = Math.min(begin + sliceSize, bytesLength);

      const bytes = new Array(end - begin);
      for (let offset = begin, i = 0; offset < end; ++i, ++offset) {
        bytes[i] = byteCharacters[offset].charCodeAt(0);
      }
      byteArrays[sliceIndex] = new Uint8Array(bytes);
    }
    return new Blob(byteArrays, { type: contentType });
  }
}
