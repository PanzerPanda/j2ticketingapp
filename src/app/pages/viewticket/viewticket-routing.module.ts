import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewticketPage } from './viewticket.page';

const routes: Routes = [
    { path: '', redirectTo: 'view', pathMatch: 'full' },
    { path: 'view', loadChildren: () => import('./view/view.module').then(m => m.ViewPageModule) },
    { path: 'view/:ticketNo', loadChildren: () => import('./details/details.module').then(m => m.DetailsPageModule) },
    { path: 'view/:ticketNo/edit', loadChildren: () => import('./edit/edit.module').then(m => m.EditPageModule) }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ViewticketRoutingModule {}
