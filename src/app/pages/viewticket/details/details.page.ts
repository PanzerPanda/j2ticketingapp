import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { Ticket } from 'src/app/models/ticketmodel';
import { DblinkService } from 'src/app/services/dblink.service';

import { Plugins, Capacitor, CameraSource, CameraResultType } from '@capacitor/core';
import { SignaturePad } from 'angular2-signaturepad';
import { Stamp } from 'src/app/models/stampmodel';
import html2canvas from 'html2canvas';
import domtoimage from 'dom-to-image';
import { DomSanitizer } from '@angular/platform-browser';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  @ViewChild('filePicker') filePickerRef: ElementRef<HTMLInputElement>;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  @ViewChild('container') container: ElementRef;

  signaturePadOptions = {}; // passed through to szimek/signature_pad constructor

  ticket: Ticket;
  cogTotal: number;
  cogTax: number;
  total: number;
  subtotal: number;
  discount: number;
  tax: number;

  isPrinting = false;
  isLoaded = false;

  usePicker: boolean;
  stampURL: string;
  sigURL: string;

  stampSet: Stamp[];
  selectedStamp: Stamp;
  stampCustomers: string[];
  stamp64: string[];

  testimg: string;

  nefeItems: any[];
  nonnefeItems: any[];
  transports: any[];
  additives: any[];

  isAndroid = false;
  isIOS = false;
  constructor(private route: ActivatedRoute, private navCtrl: NavController, private db: DblinkService,
              private actionCtrl: ActionSheetController, private toastCtrl: ToastController,
              private platform: Platform, public sanitizer: DomSanitizer, private emailComposer: EmailComposer,
              private loadingCtrl: LoadingController, public auth: AuthenticationService) { }

  ngOnInit() {
    this.getTicket();
    if ( (this.platform.is('mobile') && !this.platform.is('hybrid')) || this.platform.is('desktop')) {
      this.usePicker = true;
    }
    console.log(this.platform.platforms());
    if(this.platform.is('ios')) {
      this.isIOS = true;
    }
    else if(this.platform.is('android') || this.platform.is('desktop')) {
      this.isAndroid = true;
    }
  }

  // Get a ticket from the database and initialize values
  getTicket() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('ticketNo')) {
        this.navCtrl.navigateBack('viewticket/view');
        return;
      }
      this.loadingCtrl.create({
        message: `Loading ticket ${paramMap.get('ticketNo')}...`
      }).then(loader => {
        loader.present();
        this.db.getTicketInfo(paramMap.get('ticketNo')).valueChanges().subscribe(ticket => {
          this.ticket = ticket;
          this.getTotal();
          this.stampURL = ticket.stamp;
          this.sigURL = ticket.signature;
          this.initStamps();
          if (this.ticket.orders) {
            this.sortProducts();
          }
          console.log(this.ticket);
          this.isLoaded = true;
        });
        loader.dismiss();
        loader.onDidDismiss().then(() => {
          if(!this.ticket.completed){
            this.setSignaturePadOptions();
          }
        });
      });
    });
  }

  // Sorts the products into lists according to where they need to display on the ticket
  sortProducts() {
    console.log('sorting products');
    this.nefeItems = [];
    this.nonnefeItems = [];
    this.transports = [];
    this.additives = [];
    console.log('orders', this.ticket.orders);
    for (const order of this.ticket.orders) {
      if (order.type === 'killtruck') {
        this.nefeItems.unshift(order);
      } else if (order.type === 'nefe') {
        this.nefeItems.push(order);
      } else if (order.type === 'nonnefe') {
        this.nonnefeItems.push(order);
      } else if (order.type === 'transport') {
        this.transports.push(order);
      } else if (order.type === 'additive') {
        this.additives.push(order);
      }
    }
    console.log('nefe', this.nefeItems);
    console.log('additives', this.additives);
    console.log('nonnefe', this.nonnefeItems);
    console.log('transports', this.transports);
  }

  // Determine if the total needed is Texas or New Mexico
  getTotal() {
    this.total = 0;
    this.cogTotal = 0;
    this.subtotal = 0;
    this.discount = 0;
    this.tax = 0;
    this.cogTax = 0;

    console.log('Starting total:', this.total);
    console.log('this.state', this.ticket.state);
    if (this.ticket.state.trim().toLowerCase() === 'texas') {
      this.txTax();
      return;
    }
    if (this.ticket.state.trim().toLowerCase() === 'new mexico') {
      this.nmTax();
      return;
    } else {
      console.log('no state found');
    }
  }

  // Initialize the stamp array to select a stamp from the dropdown menu
  initStamps() {
    this.db.stampCollection.valueChanges().subscribe(data => {
      this.stampSet = data;
      this.stampCustomers = [];
      this.stamp64 = [];

      for (const stamp of this.stampSet) {
        this.stampCustomers.push(stamp.customer);
        this.stamp64.push(stamp.stampBase64);
      }
      // Set default stamp value to current ticket's well's stamp
      if (this.ticket.stamp) {
        this.selectedStamp = this.stampSet[this.stamp64.indexOf(this.ticket.stamp)];
      } else if ( !this.ticket.stamp && this.stampCustomers.includes(this.ticket.customer) ) {
        this.selectedStamp = this.stampSet[this.stampCustomers.indexOf(this.ticket.customer)];
        this.stampURL = this.selectedStamp.stampBase64;
        this.db.ticketCollection.doc(this.ticket.ticketNo).update({
          stamp: this.stampURL
        });
      }
    });
  }

  // When the user selects a new stamp update the ticket's stamp field in the database
  setStamp(event) {
    console.log(event);
    this.db.ticketCollection.doc(this.ticket.ticketNo).update({
      stamp: event.value.stampBase64
    });
    this.stampURL = event.value.stampBase64;
  }

  // Initialize the options for the signature pad
  setSignaturePadOptions() {
    this.signaturePad.set('maxWidth', 1);
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.set('penColor', 'rgb(0, 0, 0)');
    this.signaturePad.set('canvasWidth', this.container.nativeElement.clientWidth);
    this.signaturePad.set('canvasHeight', this.container.nativeElement.clientHeight);
    this.signaturePad.clear();
  }

  // Prompt the user if they want to copelte a ticket
  driverComplete() {
    this.actionCtrl.create({
      header: 'Complete ticket? This can not be undone.',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.selectTicket();
          }
        },
        {
          text: 'No',
          role: 'cancel'
        }
      ]
    }).then(alerter => {
      alerter.present();
    });
  }

  // Complete a ticket based on the platform that the app is running on.
  selectTicket() {
    this.db.ticketCollection.doc(this.ticket.ticketNo).update({
      completed: true
    });
    this.isPrinting = true;
    if(this.platform.is('ios')){
      this.iOSTicket();
      return;
    }
    this.androidTicket();
  }

  // Change a ticket to complete and route the user to the ticket list
  androidTicket() {
    const node = document.getElementById('ticket');
    domtoimage.toPng(node)
    .then(dataURL => {
      this.testimg = dataURL;
      const attachment: string = this.testimg.replace('data:image/png;base64,', 'base64:ticket.png//');
      this.openEmail(attachment);
    });
  }

  // Change a ticket to complete and route the user to the ticket list
  iOSTicket() {
    const node = document.getElementById('iosTicket');
    const ticket = document.getElementById('ticket');
    html2canvas(node, {height: ticket.offsetHeight}) // A4 page width, h: 3508 x w: 2480
    .then(dataURL => {
      this.testimg = dataURL.toDataURL();
      const attachment: string = this.testimg.replace('data:image/png;base64,', 'base64:ticket.png//');
      this.openEmail(attachment);
    });
  }

  // Take the passed completed ticket image, attached it to an email, and open default email app
  openEmail(attachment: string) {
    this.emailComposer.open({
      to: this.ticket.email,
      // cc: 'Jesus.r.garcia9191@gmail.com',
      attachments: [attachment],
      subject: `${this.ticket.ticketNo}`
    })
    .then(() => {
      this.toastCtrl.create({
        message: `Ticket ${this.ticket.ticketNo} completed.`,
        duration: 2000
      })
      .then( toast => {
        toast.present();
      })
      .then(() => {
        this.navCtrl.navigateBack('home');
      });
    });
  }

  // Calculate the total based on Texas Tax rules
  // - tax applies to each line item
  // - tax does not apply to operator costs or mileage
  txTax() {
    console.log('Texas Tax');

    // calculate the cost of product + tax
    for (const item of this.ticket.orders) {
      // cog items are not discounted so cogTax/cogTotal are calculated and added after discounts are applied
      // if items are of type transport, tax does not apply and is not calculated for the item
      // for all other items, subtotal/tax are calculated normally
      if (item.cog && item.type !== 'transport') {
        console.log(item.item + ':', item.cost, '+', item.cost * this.ticket.tax);
        this.cogTax += item.cost * this.ticket.tax;
        this.cogTotal += item.cost;
      } else if (item.type === 'transport') {
        if (item.cog) {
          this.cogTotal += item.cost;
        } else {
          this.subtotal += item.cost;
        }
        console.log(item.item + ':', item.cost);
      } else {
        this.tax += item.cost * this.ticket.tax;
        this.subtotal += item.cost;
        console.log(item.item + ':', item.cost, '+', item.cost * this.ticket.tax);
      }
    }

    console.log('cogTotal:', this.cogTotal);
    console.log('cogTax:', this.cogTax);
    console.log('Sub-total:', this.subtotal);

    console.log(`Tax:`, this.tax);
    this.tax = (this.tax * (1 - this.ticket.discount)) + this.cogTax;
    console.log(`Discounted Tax:`, this.tax);

    // If needed calculate discount amount.
    // If discounted calculate total by
    // - subtracting discount from subtotal
    // - add in taxes
    // - add in cogTotal
    // If not discounted
    // - add taxes to subtotal
    // - add in cogTotal
    console.log('Discount%:', this.ticket.discount * 100 + '%');
    if (this.ticket.discount > 0 && this.ticket.discount <= 1) {
      this.discount = this.subtotal * this.ticket.discount;
      console.log('Discount:', this.discount);
      this.total = this.subtotal - this.discount + this.tax;
      this.total += this.cogTotal;
    } else {
      this.total = this.subtotal + this.tax;
      this.total += this.cogTotal;
    }

    console.log('Total', this.total.toFixed(2));
  }

  // Calculate the total based on New Mexico Tax rules
  // - tax is based on subtotal of all items
  // - labor included in tax
  nmTax() {
    console.log('New Mexico Tax');
    // cog items are not discounted so cogTax/cogTotal are calculated and added after discounts are applied
    // for all other items, subtotal/tax are calculated normally
    for (const item of this.ticket.orders) {
      if (item.cog) {
        this.cogTotal += item.cost;
      } else {
        this.subtotal += item.cost;
      }
      console.log(item.item + ':', item.cost);
    }

    console.log('cogTotal:', this.cogTotal);
    console.log('Sub-total:', this.subtotal.toFixed(2));

    // If needed calculate discount amount.
    // If discounted calculate total by
    // - subtracting discount from subtotal
    // - add in cogTotal
    // If not discounted
    // - add in cogTotal
    console.log('Discount', this.ticket.discount * 100 + '%');
    if (this.ticket.discount > 0 && this.ticket.discount < 1) {
      this.discount = this.subtotal * this.ticket.discount;
      this.total = this.subtotal - this.discount + this.cogTotal;
    } else {
      this.total = this.subtotal + this.cogTotal;
    }

    // Calculate total taxes for both normal, transport, and cog items
    // Add taxes to total
    this.tax = this.total * this.ticket.tax;
    console.log('Tax:', this.tax);
    this.total += this.tax;
  }

  // Take a picture using the mobile camera or have the user upload a file on PC
  async takePicture() {
    if ( !Capacitor.isPluginAvailable('Camera') || this.usePicker) {
      this.filePickerRef.nativeElement.click();
      return;
    }
    Plugins.Camera.getPhoto({
      quality: 50,
      source: CameraSource.Camera,
      correctOrientation: false,
      height: 720,
      width: 1080,
      resultType: CameraResultType.Base64
    }).then( img => {
      this.uploadStamp('data:image/png;base64,' + img.base64String);
    }).catch(error => {
      console.log(error);
      this.toastCtrl.create({
        message: `The stamp image size is larger than 1Mb. Try again with a smaller image.`,
        duration: 2000
      }).then( toast => {
        toast.present();
        return true;
      });
      return false;
    });
  }

  // If the user had to select a file, read it in
  onFileChosen(event: Event) {
    console.log(event);
    const pickedFile = (event.target as HTMLInputElement).files[0];
    if (!pickedFile) {
      return;
    }
    const fr = new FileReader();
    fr.onload = () => {
      const dataUrl = fr.result.toString();
      this.uploadStamp(dataUrl.replace('data:image/jpeg;base64,', 'data:image/png;base64,'));
    };
    fr.readAsDataURL(pickedFile);
  }

  // Update stamp on the ticket and in the stamps collection
  uploadStamp(stamp64: string) {
    this.db.ticketCollection.doc(this.ticket.ticketNo).update({
      stamp: stamp64
    }).then(() => {
      // stamp was succesfully added to ticket, size confirmed as < 1Mb
      this.db.stampCollection.doc(this.ticket.customer).set({
        customer: this.ticket.customer,
        stampBase64: stamp64,
        well: this.ticket.well
      }).then( () => {
        this.stampURL = stamp64;
      });
    }).catch(error => {
      console.log(error);
      this.toastCtrl.create({
        message: `Stamp image is too large, try again with a smaller picture.`,
        duration: 2000
      }).then( toast => {
        toast.present();
        return true;
      });
    });
  }

  // Upload the signature image on szimek/signature_pad's onEnd event
  drawComplete() {
    this.db.ticketCollection.doc(this.ticket.ticketNo).update({
      signature: this.signaturePad.toDataURL()
    }).catch(error => {
      console.log(error);
      this.toastCtrl.create({
        message: `There was a problem saving the signature to the database.`,
        duration: 2000
      }).then( toast => {
        toast.present();
        return true;
      });
    });
  }

  // Will be notified of szimek/signature_pad's onBegin event
  drawStart() {
    console.log('begin drawing');
  }

  // Clear the signature field and update the image on Firebase Storage
  clearSignature() {
    this.signaturePad.clear();
    this.db.ticketCollection.doc(this.ticket.ticketNo).update({
      signature: ''
    }).catch(error => {
      console.log(error);
      this.toastCtrl.create({
        message: `There was a problem clearing the signature on the database.`,
        duration: 2000
      }).then( toast => {
        toast.present();
        return true;
      });
    });
  }

  // Captures the ticket image on the screen and calls the email function
  adminCompelte() {
    this.db.ticketCollection.doc(this.ticket.ticketNo).update({
      invoice: true
    });
    this.isPrinting = true;
    const node = document.getElementById('ticket');
    domtoimage.toPng(node).then(dataUrl => {
      this.testimg = dataUrl;
      console.log('print() methods textimg', this.testimg);
      this.email();
    }).catch(error => {
      console.error('oops, something went wrong!', error);
    });
  }

  // Pass all associated information to the email composer
  email() {
    const attachment: string = this.testimg.replace('data:image/png;base64,', 'base64:ticket.png//');
    console.log('cleaned attachment', attachment);
    console.log('modified attachment', 'base64:ticket.png//' + attachment);
    this.emailComposer.open({
      to: 'Jdesmarais1003@hotmail.com',
      // cc: 'Jesus.r.garcia9191@gmail.com',
      attachments: [attachment],
      subject: `${this.ticket.ticketNo}`
    }).then(() => {
      this.isPrinting = false;
    }).catch(error => {
      console.log('Issue sending email', error);
    });
  }
}
