import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { ViewticketPage } from './viewticket.page';
import { ViewticketRoutingModule } from './viewticket-routing.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ViewticketRoutingModule
  ],
  declarations: [ViewticketPage]
})
export class ViewticketPageModule {}
