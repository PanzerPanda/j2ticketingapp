import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewticketPage } from './viewticket.page';

describe('ViewticketPage', () => {
  let component: ViewticketPage;
  let fixture: ComponentFixture<ViewticketPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewticketPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewticketPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
