import { Component, OnInit } from '@angular/core';
import { Ticket } from 'src/app/models/ticketmodel';
import { DblinkService } from 'src/app/services/dblink.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-view',
  templateUrl: './view.page.html',
  styleUrls: ['./view.page.scss'],
})
export class ViewPage implements OnInit {

  tickets: Ticket[];
  loaded = false;

  constructor(private db: DblinkService, private loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.loaded = false;
    this.getTickets();
  }

  public getTickets() {
    this.loadingCtrl.create({
      spinner: 'crescent',
      message: 'Pulling Tickets...',
    }).then(loader => {
      loader.present();
      this.db.uncompleteticketCollection.valueChanges().subscribe(data => {
        this.tickets = data;
        this.loaded = true;
        loader.dismiss();
      });
    });
  }
}
