import { Component, OnInit } from '@angular/core';
import { DblinkService } from 'src/app/services/dblink.service';
import { Product } from 'src/app/models/productmodel';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NavController, ToastController, AlertController } from '@ionic/angular';
import { Order } from 'src/app/models/ticketmodel';
import { FieldRep } from 'src/app/models/fieldrepmodel';
import { Customer } from 'src/app/models/customermodel';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {

  customers: Customer[] = [];
  products: Product[] = [];
  ticket: FormGroup;
  fieldReps: FieldRep[] = [];

  ticketNumber: string;

  isLoaded = false;
  fieldRepInputType = false;
  validFieldRep = true;
  originalFieldRepEmail: string;
  constructor(private db: DblinkService, private fb: FormBuilder,
              private route: ActivatedRoute, private navCtrl: NavController,
              private toastCtrl: ToastController, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.isLoaded = false;
    this.getTicket();
    this.initProducts();
    this.initCustomers();
  }

  getTicket() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('ticketNo')) {
        this.navCtrl.navigateBack('viewticket/view');
        return;
      }
      this.db.getTicketInfo(paramMap.get('ticketNo')).valueChanges().subscribe(data => {
        this.ticket = this.fb.group({
          ticketNo: [data.ticketNo, Validators.required],
          customer: [data.customer, Validators.required],
          afe: [data.afe, Validators.required],
          fieldrep: [data.fieldrep, Validators.required],
          well: [data.well, Validators.required],
          orders: this.fb.array([]),
          email: [data.email],
          seller: [data.seller],
          discount: [data.discount]
        });
        this.ticketNumber = data.ticketNo;
        this.initFieldReps();
        if (data.email) {
          this.originalFieldRepEmail = data.email;
        }

        this.ticket.get('ticketNo').disable();
        this.setOrders(data.orders);
        console.log(this.ticket.value);
        this.isLoaded = true;
      });
    });
  }

  // grab the products from the database and store them in a locally accessable array
  public initProducts() {
    this.db.productsCollection.valueChanges().subscribe(data => {
      this.products = data;
    });
  }

  // grab customers from the database and store them in a locally accessable array
  public initCustomers() {
    this.db.customerCollection.valueChanges().subscribe(data => {
      this.customers = data;
    });
  }

  // Alert user to the number of found field reps for the customer
  public initFieldReps() {
    this.db.getCustomerFieldReps(this.ticket.get('customer').value);
    this.db.customerFieldRepCollection.valueChanges().subscribe(reps => {
      this.fieldReps = reps;
      if (this.fieldReps.length === 0) {
        this.showErrorToast(`No Field Reps available for ${this.ticket.get('customer').value}.`);
      } else {
        this.showErrorToast(`Found ${this.fieldReps.length} Field Rep(s) for ${this.ticket.get('customer').value}.`);
      }
      this.checkFieldRepInputType();
    });
  }

  // get method used for populating the dynamically generated form array for each item order
  get getOrders() {
    return this.ticket.get('orders') as FormArray;
  }

  setOrders(orderList: Order[]) {
    console.log(orderList);
    for (const order of orderList) {
      this.getOrders.push(this.setOrder(order));
    }
  }

  // creates the form group representing a single order
  setOrder(order) {
    console.log(order);
    if (order.type === 'additive') {
      return this.fb.group({
        item: order.item,
        quantity: [{value: order.quantity, disabled: true}],
        perUnit: order.perUnit,
        cost: order.cost,
        type: order.type,
        cog: order.cog,
        perThousand: order.perThousand
      });
    } else {
      return this.fb.group({
        item: order.item,
        quantity: order.quantity,
        perUnit: order.perUnit,
        cost: order.cost,
        type: order.type,
        cog: order.cog
      });
    }
  }

  // Switches the field rep input type between dropdown and input text
  toggleFieldRep() {
    this.fieldRepInputType = !this.fieldRepInputType;
    this.checkFieldRep();
    console.log(this.fieldRepInputType);
  }

  // Add a field rep email
  setSelectedFieldRepEmail() {
    const selectedFieldRep = this.fieldReps.find(fieldrep => {
      return fieldrep.name = this.ticket.get('fieldrep').value;
    });
    this.ticket.patchValue({
      email: selectedFieldRep.email
    });
    this.originalFieldRepEmail = selectedFieldRep.email;
  }

  // check if the tickets field rep already belongs to the customer on the database
  checkFieldRepInputType() {
    const ticketFieldRep = this.fieldReps.find(fieldrep => {
      return fieldrep.name === this.ticket.get('fieldrep').value;
    });
    if (ticketFieldRep) { // it does so set the fieldRepInputType to dropdown
      this.fieldRepInputType = true;
    } // else it doesn't leave it as text input
  }

  // select a given order[index] selected product
  public getProduct(index: number): Product {
    // console.log(this.getOrders.controls[index].value.item);
    for (const product of this.products) {
      if (product.code === this.getOrders.controls[index].value.item ) {
        return product;
      }
    }
  }

  // adjust the cost of an order item based on the item's quantity
  public updateCost(index: number) {
    // console.log(this.getOrders.controls[index].value.item);
    const product = this.getProduct(index);
    console.log('updated price', parseFloat((product.retailprice * this.getOrders.controls[index].value.quantity).toFixed(2)));
    this.getOrders.controls[index].patchValue({
      cost: parseFloat((product.retailprice * this.getOrders.controls[index].value.quantity).toFixed(2)),
      perUnit: product.retailprice
    });
  }

  // set the current customer and updates various fields based on customer selection
  public updateCustomerFields() {
    const selectedCustomer = this.customers.find( customer => {
      return customer.name === this.ticket.get('customer').value;
    });
    this.ticket.patchValue({
      fieldrep: '',
      email: '',
      seller: selectedCustomer.seller,
      discount: selectedCustomer.discount
    });
    this.initFieldReps();
    console.log(this.ticket.value);
  }

  //verify field rep name does not match customer
  checkFieldRep() {
    let fR = this.ticket.get('fieldrep').value as string;
    fR = fR.toLowerCase().trim();
    let cust = this.ticket.get('customer').value as string;
    cust = cust.toLowerCase().trim();
    console.log('fieldRep', fR);
    console.log('customer', cust);
    if (fR === cust) {
      this.showErrorToast('Field Rep name cannot match customer name.');
      this.validFieldRep = false;
    } else {
      this.validFieldRep = true;
     }
  }

  // validate a entered email address format
  validateEmail(email) {
    if (email === 'placeholder@gmail.com') {
      this.showErrorToast('placeholder@gmail.com is not a valid email address.');
      return false;
    }
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      return true;
    } else {
      this.showErrorToast(`${email} is not a valid email address`);
      return false;
    }
  }

  // add a field rep to a customers list of field reps
  addFieldRepDatabase(email) {
    if (this.validateEmail(email)) {
      console.log(`${email} is a valid email address`);
      this.db.fieldRepCollection.doc(this.ticket.get('fieldrep').value).set({
        customer: this.ticket.get('customer').value,
        email: email,
        name: this.ticket.get('fieldrep').value
      });
      this.updateTicket();
    } else {
      return true;
    }
  }

  addFieldRepTicket(email) {
    if (this.validateEmail(email)) {
      this.ticket.patchValue({
        email: email,
        name: this.ticket.get('fieldrep').value
      });
      this.updateTicket();
    } else {
      return true;
    }
  }

  // update a field rep email in the field rep collection
  updateFieldRepDatabase(frEmail){ 
    if (this.validateEmail(frEmail)) {
      console.log(`${frEmail} is a valid email address`);
      this.db.fieldRepCollection.doc(this.ticket.get('fieldrep').value).update({
        email: frEmail
      });
      this.updateTicket();
    } else {
      return true;
    }
  }

  addFieldRepAlert() {
    const frEmail: string = this.ticket.get('email').value.trim();
    this.alertCtrl.create({
      header: 'New Field Rep',
      message: `How do you want to handle:<br/>
                Name: ${this.ticket.get('fieldrep').value}<br/>
                Email: ${this.ticket.get('email').value}`,
      buttons: [
        {
          text: `Add as field rep for ${this.ticket.get('customer').value}`,
          handler: () => {
            this.addFieldRepDatabase(frEmail); // update field rep collection
          }
        },
        {
          text: `Only use for ticket ${this.ticket.get('ticketNo').value}`,
          handler: () => {
            this.addFieldRepTicket(frEmail); // only add field rep to ticket
          }
        },
        {
          text: 'Cancel ',
          role: 'cancel',
          cssClass: 'secondary'
        }
      ]
    }).then(alertPrompt => {
      alertPrompt.present();
    });
  }

  updateFieldRepAlert() {
    const frEmail: string = this.ticket.get('email').value.trim();
    this.alertCtrl.create({
      header: 'New Field Rep Email?',
      message: `How do you want to update the email for ${this.ticket.get('fieldrep').value}<br/>
                from \"${this.originalFieldRepEmail}\" to
                \"${this.ticket.get('email').value}\"? <br/>.`,
      buttons: [
        {
          text: `Update ${this.ticket.get('fieldrep').value} on database`,
          handler: () => {
            this.updateFieldRepDatabase(frEmail);
          }
        },
        {
          text: `Update ${this.ticket.get('fieldrep').value} on ticket only`,
          handler: () => {
            this.addFieldRepTicket(frEmail);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary'
        }
      ]
    }).then(alertPrompt => {
      alertPrompt.present();
    });
  }

  // display toast error messages
  showErrorToast(data: any) {
    this.toastCtrl.create({
      message: data,
      duration: 2000
    }).then(toaster => {
      toaster.present();
    });
  }

  // enables the additives quantity field before updating the ticket
  enableAdditiveQuantity() {
    for (const order of this.getOrders.controls) {
      console.log(order);
      if (order.value.type === 'additive') {
        order.get('quantity').enable();
      }
    }
  }

  updateTicket() {
    this.enableAdditiveQuantity();
    if (this.validateEmail(this.ticket.get('email').value.trim())) {
      this.db.ticketCollection.doc(this.ticket.get('ticketNo').value).update({
        ticketNo: this.ticket.get('ticketNo').value,
        customer: this.ticket.get('customer').value,
        afe: this.ticket.get('afe').value,
        fieldrep: this.ticket.get('fieldrep').value,
        well: this.ticket.get('well').value,
        orders: this.ticket.get('orders').value,
        email: this.ticket.get('email').value.trim(),
        seller: this.ticket.get('seller').value,
        discount: this.ticket.get('discount').value
      });
      this.toastCtrl.create({
        message: `Ticket ${this.ticketNumber} updated.`,
        duration: 2000
      }).then(toaster => {
        toaster.present();
        this.navCtrl.navigateBack(`/viewticket/view/${this.ticketNumber}`);
      });
    } else {
      this.showErrorToast(`\"${this.ticket.get('email').value.trim()}\" is not a valid email address.
                           Ticket ${this.ticketNumber} not updated`);
    }
  }

  // submit the ticket to the database
  public onSubmit() {
    console.log(this.ticket);
    if (!this.fieldRepInputType) {
      this.addFieldRepAlert();
    } else if (this.originalFieldRepEmail !== this.ticket.controls.email.value.trim()) {
        this.updateFieldRepAlert();
    } else {
      this.updateTicket();
    }
  }
}
