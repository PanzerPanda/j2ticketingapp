import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewvehiclePage } from './newvehicle.page';

const routes: Routes = [
  {
    path: '',
    component: NewvehiclePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewvehiclePageRoutingModule {}
