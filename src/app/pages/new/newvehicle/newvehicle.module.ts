import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicSelectableModule } from 'ionic-selectable';

import { IonicModule } from '@ionic/angular';

import { NewvehiclePageRoutingModule } from './newvehicle-routing.module';

import { NewvehiclePage } from './newvehicle.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    NewvehiclePageRoutingModule,
    IonicSelectableModule
  ],
  declarations: [NewvehiclePage]
})
export class NewvehiclePageModule {}
