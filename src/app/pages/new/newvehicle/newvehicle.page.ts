import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DblinkService } from '../../../services/dblink.service';
import { ToastController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs/internal/Observable';
import { take } from 'rxjs/internal/operators/take';

@Component({
  selector: 'app-newvehicle',
  templateUrl: './newvehicle.page.html',
  styleUrls: ['./newvehicle.page.scss'],
})
export class NewvehiclePage implements OnInit {

  newVehicle: FormGroup;
  editVehicle: FormGroup;

  editMode = false;
  vehicles: Observable<any>;
  constructor(private db: DblinkService, private fb: FormBuilder, private toastCtrl: ToastController,
              private alertCtrl: AlertController) { }

  ngOnInit() {
    this.initForm();
    this.initTrucks();
  }

  // initialize the editTruck form controls
  initForm() {
    this.newVehicle = this.fb.group({
      vehicleno: ['', Validators.required],
      type: ['', Validators.required]
    });
  }

  initEditForm() {
    this.editVehicle = this.fb.group({
      vehicleno: ['', Validators.required],
      type: ['', Validators.required]
    });
  }

  initTrucks() {
    this.vehicles = this.db.vehicleCollection.valueChanges();
  }

  changeMode() {
    this.editMode = !this.editMode;
    this.editMode ? this.initEditForm() : this.initForm();
  }

  updateFields() {
    this.db.getVehicleInfo(this.editVehicle.get('vehicleno').value).valueChanges().pipe(take(1)).subscribe( selectedVehicle => {
      this.editVehicle.patchValue({
        vehicleno: selectedVehicle.vehicleno,
        type: selectedVehicle.type
      });
    });
  }

  onSubmit() {
    const vehicleno = this.newVehicle.get('vehicleno').value;
    this.db.getVehicleInfo(vehicleno).valueChanges().pipe(take(1)).subscribe( vehicleCheck => {
      // If the base variant of a product exists, confirm the user wants to overwrite the value
      if(vehicleCheck) { 
        this.alertCtrl.create({
          header: `${vehicleno} already exists`,
          message: `${vehicleno} already exists in the database. Do you want
          to over write it?`,
          buttons: [
            {
              text: 'Confirm',
              handler: () => {
                this.db.vehicleCollection.doc(vehicleno).set(this.newVehicle.value);
                this.initForm();
                this.showToast(`${vehicleno} added.`); 
              }
            },
            {
              text: 'Cancel',
              role: 'cancel'
            }
          ]
        }).then(alerter => {alerter.present()});
      } 
      else {
        this.db.vehicleCollection.doc(vehicleno).set(this.newVehicle.value);
        this.initForm();
        this.showToast(`${vehicleno} added.`); 
      }
    });
  }

  onUpdate() {
    console.log(this.editVehicle.value);
    console.log('vehicle')
    this.db.vehicleCollection.doc(this.editVehicle.get('vehicleno').value).update({
      vehicleno: this.editVehicle.get('vehicleno').value,
      type: this.editVehicle.get('type').value
    });
    this.showToast(`${this.editVehicle.get('vehicleno').value} updated.`);
  }

  delete() {
    this.alertCtrl.create({
      header: 'Warning!',
      message: `Are you sure you want to delete ${this.editVehicle.get('vehicleno').value} from the database?<br/>
      This cannot be undone.`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Confirm Deletion',
          handler: () => {
            this.db.vehicleCollection.doc(this.editVehicle.get('vehicleno').value).delete();
            this.initEditForm();
          }
        }
      ]
    }).then(alerter => { alerter.present(); });
  }

  // display a toast message
  showToast(data: any) {
    this.toastCtrl.create({
      message: data,
      duration: 2000
    }).then(toaster => {
      toaster.present();
    });
  }

  // reset the form
  public reset() {
    this.initForm();
  }
}
