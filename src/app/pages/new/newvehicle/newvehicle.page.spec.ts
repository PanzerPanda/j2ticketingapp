import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewvehiclePage } from './newvehicle.page';

describe('NewvehiclePage', () => {
  let component: NewvehiclePage;
  let fixture: ComponentFixture<NewvehiclePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewvehiclePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewvehiclePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
