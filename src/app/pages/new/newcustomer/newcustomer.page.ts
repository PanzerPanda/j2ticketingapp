import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DblinkService } from '../../../services/dblink.service';
import { ToastController, AlertController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-newcustomer',
  templateUrl: './newcustomer.page.html',
  styleUrls: ['./newcustomer.page.scss'],
})
export class NewcustomerPage implements OnInit {

  newCustomer: FormGroup;
  editCustomer: FormGroup;
  edit = false;
  customers: Observable<any>
  constructor(private db: DblinkService, private fb: FormBuilder, private toastCtrl: ToastController,
              private alertCtrl: AlertController) { }

  ngOnInit() {
    this.initForm();
    this.initCustomers();
  }

  // initialize the newCustomer form controls
  initForm() {
    this.newCustomer = this.fb.group({
      discount: [0, Validators.required],
      name: ['', Validators.required],
      seller: ['Joey', Validators.required],
      state: ['Texas', Validators.required],
      tax: [0.0825, Validators.required]
    });
  }

  initEditForm() {
    this.editCustomer = this.fb.group({
      discount: [0, Validators.required],
      name: ['', Validators.required],
      seller: ['Joey', Validators.required],
      state: ['Texas', Validators.required],
      tax: [0.0825, Validators.required]
    });
  }

  initCustomers() {
    this.customers = this.db.customerCollection.valueChanges();
  }

  // Update discount based on whole or decmial value
  updateDiscount() {
    const formGroup = this.edit ? this.editCustomer : this.newCustomer;
    const discount = formGroup.get('discount').value;
    if (discount >= 1) {
      formGroup.get('discount').setValue(discount / 100);
      return;
    }
    if (0 < discount && discount < 1) {
      formGroup.get('discount').setValue(discount);
      return;
    }
    formGroup.get('discount').setValue(0);
  }

  changeMode() {
    this.edit = !this.edit;
    this.edit ? this.initEditForm() : this.initForm();
  }

  updateFields(): any {
    this.db.getCustomerInfo(this.editCustomer.get('name').value).valueChanges().pipe(take(1)).subscribe( selectedCustomer => {
      this.editCustomer.patchValue({
        discount: selectedCustomer.discount,
        name: selectedCustomer.name,
        seller: selectedCustomer.seller,
        state: selectedCustomer.state,
        tax: selectedCustomer.tax
      });
    });
  }

  // submit the ticket to the database
  public onSubmit() {
    this.updateDiscount();
    console.log(this.newCustomer.value);

    this.db.getCustomerInfo(this.newCustomer.get('name').value).valueChanges().pipe(take(1)).subscribe( customerCheck => {
      // If the base variant of a product exists, confirm the user wants to overwrite the value
      if(customerCheck) { 
        this.alertCtrl.create({
          header: `${this.newCustomer.get('name').value} already exists`,
          message: `${this.newCustomer.get('name').value} already exists in the database. Do you want
          to over write it?`,
          buttons: [
            {
              text: 'Confirm',
              handler: () => {
                this.db.customerCollection.doc(this.newCustomer.get('name').value).set(this.newCustomer.value, {merge: true}).then(() => {
                  this.toastCtrl.create({
                    message: `${this.newCustomer.get('name').value} added.`,
                    duration: 2000
                  }).then(toaster => {
                    toaster.present();
                  });
                });
              }
            },
            {
              text: 'Cancel',
              role: 'cancel'
            }
          ]
        }).then(alerter => {alerter.present()});
      }
      else {
        this.db.customerCollection.doc(this.newCustomer.get('name').value).set(this.newCustomer.value).then(() => {
          this.toastCtrl.create({
            message: `${this.newCustomer.get('name').value} added.`,
            duration: 2000
          }).then(toaster => {
            toaster.present();
          });
        });
      }
    });
  }

  onUpdate() {
    this.updateDiscount();
    this.db.customerCollection.doc(this.editCustomer.get('name').value).update({
      discount: this.editCustomer.get('discount').value,
      name: this.editCustomer.get('name').value,
      seller: this.editCustomer.get('seller').value,
      state: this.editCustomer.get('state').value,
      tax: this.editCustomer.get('tax').value
    });
    this.toastCtrl.create({
      message: `${this.editCustomer.get('name').value} updated.`,
      duration: 2000
    }).then(toaster => {
      toaster.present();
    });
  }

  delete() {
    this.alertCtrl.create({
      header: 'Warning!',
      message: `Are you sure you want to delete ${this.editCustomer.get('name').value} from the database?<br/>
      This cannot be undone.`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Confirm Deletion',
          handler: () => {
            this.db.customerCollection.doc(this.editCustomer.get('name').value).delete();
            this.initEditForm();
          }
        }
      ]
    }).then(alerter => { alerter.present(); });
  }

  // reset the form
  public reset() {
    console.log(this.newCustomer.value);
    this.initForm();
  }
}
