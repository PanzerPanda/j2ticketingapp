import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.page.html',
  styleUrls: ['./newuser.page.scss'],
})
export class NewuserPage implements OnInit {

  newUser: FormGroup;
  validEmail = true;
  validLogin = true;

  constructor(public auth: AuthenticationService, private fb: FormBuilder, private router: Router,
              private toastCtrl: ToastController) { }

  ngOnInit() {
    this.initRegistrationForm();
  }

  get newId() { return this.newUser.get('id').value; }
  get newPassword() { return this.newUser.get('password').value; }
  get displayName() { return this.newUser.get('displayName').value; }
  get role() { return this.newUser.get('role').value; }

  initRegistrationForm() {
    this.newUser = this.fb.group({
      id: ['', Validators.required],
      password: ['', Validators.minLength(6)],
      displayName: [''],
      role: [''],
      fullEmail: false
    });
  }

  validateEmail(email) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      return this.validEmail = true;
    }
    return this.validEmail = false;
  }

  verifyLogin() {
    const email:string = this.newUser.get('id').value.trim();
    if(this.newUser.get('fullEmail').value === true) { 
      this.validLogin = true;
      if(!this.validateEmail(email)){
        this.validEmail = false;
        return;
      } else {
        this.validEmail = true;
        return;
      }
    } else if (this.newUser.get('fullEmail').value === false) {
      this.validEmail = true;
      if(email.includes('@')){
        this.validLogin = false;
        return;
      }
    }
    this.validEmail = true;
    this.validLogin = true;
  }

  onSubmit() {
    const email:string = this.newUser.get('id').value.trim();
    if(this.newUser.get('fullEmail').value === true) {
      console.log('full email checked.');
      console.log('checking email:', email);
      if(this.validateEmail(email)){
        console.log('registering with real email address');
        this.auth.registerRealEmail(this.newId, this.newPassword, this.displayName, this.role);
      }
      else {
        console.log('false email');
        return;
      }
      
    } else {
      console.log('registering with pseudo email username');
      this.auth.registerPseudoEmail(this.newId, this.newPassword, this.displayName, this.role);
    }
  }

  // Display toast messages.
  showErrorToast(data: any) {
    this.toastCtrl.create({
      message: data,
      duration: 2000
    }).then(toaster => {
      toaster.present();
    });
  }
}
