import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DblinkService } from '../../../services/dblink.service';
import { ToastController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-newcounty',
  templateUrl: './newcounty.page.html',
  styleUrls: ['./newcounty.page.scss'],
})
export class NewcountyPage implements OnInit {

  newCounty: FormGroup;
  editCounty: FormGroup;
  edit = false;

  counties: Observable<any>;

  constructor(private db: DblinkService, private fb: FormBuilder, private toastCtrl: ToastController,
              private alertCtrl: AlertController) { }

  ngOnInit() {
    this.initForm();
    this.initCounties();
  }

  // initialize the newCounty form controls
  initForm() {
    this.newCounty = this.fb.group({
      name: ['', Validators.required],
      state: ['Texas', Validators.required],
      tax: [0.0825, Validators.required]
    });
  }

  initEditForm() {
    this.editCounty = this.fb.group({
      name: ['', Validators.required],
      state: ['Texas', Validators.required],
      tax: [0.0825, Validators.required]
    });
  }

  initCounties() {
    this.counties = this.db.countyCollection.valueChanges();
  }

  changeMode() {
    this.edit = !this.edit;
    this.edit ? this.initEditForm() : this.initForm();
  }

  updateFields(): any {
    this.db.getCountyInfo(this.editCounty.get('name').value).valueChanges().pipe(take(1)).subscribe( selectedCounty => {
      this.editCounty.patchValue({
        name: selectedCounty.name,
        state: selectedCounty.state,
        tax: selectedCounty.tax
      });
    });
  }

  // submit the ticket to the database
  public onSubmit() {
    console.log(this.newCounty.value);
    this.db.getCountyInfo(this.newCounty.get('name').value).valueChanges().pipe(take(1)).subscribe( countyCheck => {
      // If the base variant of a product exists, confirm the user wants to overwrite the value
      if(countyCheck) { 
        this.alertCtrl.create({
          header: `${this.newCounty.get('name').value} already exists`,
          message: `${this.newCounty.get('name').value} already exists in the database. Do you want
          to over write it?`,
          buttons: [
            {
              text: 'Confirm',
              handler: () => {
                this.db.countyCollection.doc(this.newCounty.get('name').value).set(this.newCounty.value);
                this.initForm();
                this.toastCtrl.create({
                  message: `${this.newCounty.get('name').value} Added.`,
                  duration: 2000
                }).then(toaster => {
                  toaster.present();
                }); 
              }
            },
            {
              text: 'Cancel',
              role: 'cancel'
            }
          ]
        }).then(alerter => {alerter.present()});
      } 
      else {
        this.db.countyCollection.doc(this.newCounty.get('name').value).set(this.newCounty.value);
        this.initForm();
        this.toastCtrl.create({
          message: `${this.newCounty.get('name').value} Added.`,
          duration: 2000
        }).then(toaster => {
          toaster.present();
        }); 
      }
    });
  }

  onUpdate() {
    this.db.countyCollection.doc(this.editCounty.get('name').value).update({
      name: this.editCounty.get('name').value,
      state: this.editCounty.get('state').value,
      tax: this.editCounty.get('tax').value
    });
    this.toastCtrl.create({
      message: `${this.editCounty.get('name').value} updated.`,
      duration: 2000
    }).then(toaster => {
      toaster.present();
    });
  }

  delete() {
    this.alertCtrl.create({
      header: 'Warning!',
      message: `Are you sure you want to delete ${this.editCounty.get('name').value} from the database?<br/>
      This cannot be undone.`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Confirm Deletion',
          handler: () => {
            this.db.countyCollection.doc(this.editCounty.get('name').value).delete();
            this.initEditForm();
          }
        }
      ]
    }).then(alerter => { alerter.present(); });
  }

  // reset the form
  public reset() {
    console.log(this.newCounty.value);
    this.initForm();
  }
}
