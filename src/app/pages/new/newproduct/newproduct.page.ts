import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DblinkService } from '../../../services/dblink.service';
import { ToastController, AlertController } from '@ionic/angular';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/internal/Observable';
import { take } from 'rxjs/internal/operators/take';

@Component({
  selector: 'app-newproduct',
  templateUrl: './newproduct.page.html',
  styleUrls: ['./newproduct.page.scss'],
})
export class NewproductPage implements OnInit {

  newProduct: FormGroup;
  editProduct: FormGroup;

  validItem = true;
  editMode = false;
  saved = true;
  products: Observable<any>;
  constructor(private db: DblinkService, private fb: FormBuilder, private toastCtrl: ToastController,
              private alertCtrl: AlertController) { }

  ngOnInit() {
    this.initForm();
    this.initProducts();
  }

  // initialize the newProduct form controls
  initForm() {
    this.newProduct = this.fb.group({
      code: ['', Validators.required],
      costperunit: [0, Validators.required],
      description: ['', Validators.required],
      details: ['', Validators.required],
      markup: [0, Validators.required],
      multiplier: [1, Validators.required],
      priceperunit: [0, Validators.required],
      provider: ['', Validators.required],
      providercode: ['', Validators.required],
      retailprice: [0, Validators.required],
      unit: ['', Validators.required],
      type: ['', Validators.required],
      cog: [false, Validators.required]
    });
  }

  initEditForm() {
    this.editProduct = this.fb.group({
      code: ['', Validators.required],
      costperunit: [0, Validators.required],
      description: ['', Validators.required],
      details: ['', Validators.required],
      markup: [0, Validators.required],
      multiplier: [1, Validators.required],
      priceperunit: [0, Validators.required],
      provider: ['', Validators.required],
      providercode: ['', Validators.required],
      retailprice: [0, Validators.required],
      unit: ['', Validators.required],
      type: ['', Validators.required],
      cog: [false, Validators.required]
    });
  }

  typeCheck(perThousand: number = 0) {
    const formGroup = this.editMode ? this.editProduct : this.newProduct;
    if ( formGroup.get('type').value === 'additive') {
      formGroup.addControl('perThousand',
      new FormControl(Validators.required));
      formGroup.patchValue({
        perThousand: perThousand
      })
    } else {
      formGroup.removeControl('perThousand');
    }
  }

  initProducts() {
    this.products = this.db.productsCollection.valueChanges();
  }

  changeMode() {
    this.editMode = !this.editMode;
    this.editMode ? this.initEditForm() : this.initForm();
    this.saved = this.editMode;
    if(this.editMode && (this.editProduct.touched && !this.saved)) { 
      this.add();
    }
  }

  edit() {
    console.log('editMode is', !this.editMode ? 'On' : 'Off');
    this.initEditForm();
    this.editMode = !this.editMode;
    this.saved = false;
  }

  add() {
    if (this.editProduct.touched && !this.saved) {
      this.alertCtrl.create({
        header: 'Are you sure?',
        message: 'There are unsaved changes, do you wish to continue?',
        buttons: [
          {
            text: 'Continue',
            handler: () => {
              this.editMode = !this.editMode;
              this.saved = !this.saved;
            }
          },
          {
            text: 'Cancel',
            role: 'cancel'
          }
        ]
      }).then(alerter => { alerter.present(); });
    } 
  }

  updateFields(): any {
    const code = this.getCode();
    if(code != '' && code != ' COG'){
      this.db.getProductInfo(code).valueChanges().pipe(take(1)).subscribe( selectedProduct => {
        if (selectedProduct) {
          this.validItem = true;
          this.editProduct.patchValue({
            code: this.editProduct.get('code').value,
            costperunit: selectedProduct.costperunit,
            description: selectedProduct.description,
            details: selectedProduct.details,
            markup: selectedProduct.markup,
            multiplier: selectedProduct.multiplier,
            priceperunit: selectedProduct.priceperunit,
            provider: selectedProduct.provider,
            providercode: selectedProduct.providercode,
            retailprice: selectedProduct.retailprice,
            unit: selectedProduct.unit,
            type: selectedProduct.type,
            cog: selectedProduct.cog
          });
          this.typeCheck(selectedProduct.perThousand);
        } else {
          this.validItem = false;
          if(code) {
            this.addItemOnError(code);
          }
        }
      });
    }
  }

  // The item was not found in the datbase, ask user if they would like to add it
  addItemOnError(code: string) {
    this.alertCtrl.create({
      header: `${code} not found.`,
      message: `Unable to find ${code} in the database.
      Would you like to add it?`,
      buttons: [
        {
          text: `Add to database, as is`,
          handler: () => {
            this.db.productsCollection.doc(code).set(this.editProduct.value);
            this.showToast(`${code} added to database.`);
            this.validItem = true;
            return true;
          }
        },
        {
          text: 'Modify before adding',
          handler: () => {
            this.editMode = false;
            this.newProduct = this.editProduct;
          }
        },
        {
          text: 'Cancel ',
          role: 'cancel',
          cssClass: 'secondary'
        }
      ]
    }).then(alerter => { alerter.present(); });
  }

  

  // submit the ticket to the database
  public onSubmit() {
    this.db.getProductInfo(this.newProduct.get('code').value).valueChanges().pipe(take(1)).subscribe( productCheck => {
      // If the base variant of a product exists, confirm the user wants to overwrite the value
      if(productCheck && !this.newProduct.get('cog').value) { 
        this.alertCtrl.create({
          header: `${this.newProduct.get('code').value} already exists`,
          message: `${this.newProduct.get('code').value} already exists in the database. Do you want
          to over write it?`,
          buttons: [
            {
              text: 'Confirm',
              handler: () => {
                this.addItem();
              }
            },
            {
              text: 'Cancel',
              role: 'cancel'
            }
          ]
        }).then(alerter => {alerter.present()});
      }
      // The product's base variant does not exist, check if the submitted item is a COG item
      else if(!productCheck && this.newProduct.get('cog').value){ 
        this.alertCtrl.create({
          header: 'NonCOG variant not found',
          message: `A nonCOG varient of ${this.newProduct.get('code').value} must exist before a
          COG varient can be added.`,
          buttons: ['Ok']
        }).then(alerter => {alerter.present()});
      } 
      // Item is a new nonCOG item
      else {
        this.addItem();
      }
    });
  }

  // add an item to the datbase
  addItem() {
    const code = this.getCode();
    this.db.productsCollection.doc(code).set(this.newProduct.value);
    this.initForm();
    this.showToast(`${code} added.`);
  }

  onUpdate() {
    console.log('update', this.editProduct.value);
    const code = this.getCode();
    if (this.editProduct.get('type').value === 'additive') {
      this.db.productsCollection.doc(code).update({
        code,
        costperunit: this.editProduct.get('costperunit').value,
        description: this.editProduct.get('description').value,
        details: this.editProduct.get('details').value,
        markup: this.editProduct.get('markup').value,
        multiplier: this.editProduct.get('multiplier').value,
        priceperunit: this.editProduct.get('priceperunit').value,
        provider: this.editProduct.get('provider').value,
        providercode: this.editProduct.get('providercode').value,
        retailprice: this.editProduct.get('retailprice').value,
        unit: this.editProduct.get('unit').value,
        type: this.editProduct.get('type').value,
        cog: this.editProduct.get('cog').value,
        perThousand: this.editProduct.get('perThousand').value
      });
    } else {
      this.db.productsCollection.doc(code).update({
        code,
        costperunit: this.editProduct.get('costperunit').value,
        description: this.editProduct.get('description').value,
        details: this.editProduct.get('details').value,
        markup: this.editProduct.get('markup').value,
        multiplier: this.editProduct.get('multiplier').value,
        priceperunit: this.editProduct.get('priceperunit').value,
        provider: this.editProduct.get('provider').value,
        providercode: this.editProduct.get('providercode').value,
        retailprice: this.editProduct.get('retailprice').value,
        unit: this.editProduct.get('unit').value,
        type: this.editProduct.get('type').value,
        cog: this.editProduct.get('cog').value,
        perThousand: firebase.firestore.FieldValue.delete()
      });
    }
    this.showToast(`${code} updated.`);
    this.editProduct.markAsUntouched();
    this.saved = true;
  }

  delete() {
    const code = this.getCode();
    this.alertCtrl.create({
      header: 'Warning!',
      message: `Are you sure you want to delete ${code} from the database?<br/>
      This cannot be undone.`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Confirm Deletion',
          handler: () => {
            this.db.productsCollection.doc(code).delete();
            this.initEditForm();
          }
        }
      ]
    }).then(alerter => { alerter.present(); });
  }

  // display a toast message
  showToast(data: any) {
    this.toastCtrl.create({
      message: data,
      duration: 2000
    }).then(toaster => {
      toaster.present();
    });
  }

  // return the code of a given item
  getCode(): string {
    const formGroup = this.editMode ? this.editProduct : this.newProduct;
    let code = formGroup.get('code').value.trim();
    if (formGroup.get('cog').value) {
      code += ' COG';
    }
    console.log('sending back code', code);
    return code;
  }

  // reset the form
  public reset() {
    console.log(this.newProduct.value);
    this.initForm();
  }
}
