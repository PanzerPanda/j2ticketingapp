import { Component, OnInit } from '@angular/core';
import { Ticket } from 'src/app/models/ticketmodel';
import { DblinkService } from 'src/app/services/dblink.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-completetickets',
  templateUrl: './completetickets.page.html',
  styleUrls: ['./completetickets.page.scss'],
})
export class CompleteticketsPage implements OnInit {

  tickets: Ticket[];
  constructor(private db: DblinkService, private loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.getCompletedTickets();
  }

  public getCompletedTickets() {
    this.loadingCtrl.create({
      spinner: 'crescent',
      message: 'Pulling Tickets...',
    }).then(loader => {
      loader.present();
      this.db.completeticketCollection.valueChanges().subscribe(data => {
        this.tickets = data;
        loader.dismiss();
      });
    });
  }

}
