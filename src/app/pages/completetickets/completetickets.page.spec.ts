import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteticketsPage } from './completetickets.page';

describe('CompleteticketsPage', () => {
  let component: CompleteticketsPage;
  let fixture: ComponentFixture<CompleteticketsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteticketsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteticketsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
