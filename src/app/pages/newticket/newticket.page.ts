import { Component, OnInit } from '@angular/core';
import { DblinkService} from 'src/app/services/dblink.service';
import { formatDate } from '@angular/common';
import { Observable } from 'rxjs';
import { Customer } from '../../models/customermodel';
import { Product } from '../../models/productmodel';
import { County } from '../../models/countymodel';
import { Vehicle } from 'src/app/models/vehiclemodel';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { ActionSheetController, NavController, AlertController, ToastController } from '@ionic/angular';
import { take } from 'rxjs/internal/operators/take';

@Component({
  selector: 'app-newticket',
  templateUrl: './newticket.page.html',
  styleUrls: ['./newticket.page.scss'],
})

export class NewticketPage implements OnInit {

  // customers and products initialized to prevent
  // "Cannot read property 'find' of undefined" error in <ionic-selectable>
  customers: Customer[] = [];
  products: Product[] = [];
  counties: County[];

  newTicket: FormGroup;
  trucks: Vehicle[] = [];
  trailers: Observable<any>;

  constructor(private db: DblinkService, private fb: FormBuilder, private actionCtrl: ActionSheetController,
              private navCtrl: NavController, private alertCtrl: AlertController, private toastCtrl: ToastController) {}

  ngOnInit() {
    this.initForm();
    this.initCustomers();
    this.initProducts();
    this.initCounties();
    this.initDropdowns();
  }

  // set the current date and generate a blank new ticket model
  public initForm() {
    this.newTicket = this.fb.group({
      ticketNo: ['', Validators.required],
      afe: [''],
      customer: ['', Validators.required],
      discount: [''],
      state: ['', Validators.required],
      county: ['', Validators.required],
      well: [''],
      date: [''],
      seller: [''],
      truck: [''],
      trailer: [''],
      tax: [0.0825, Validators.required],
      directions: [''],
      specialdirections: [''],
      completed: [false],
      address: [''],
      fieldrep: [''],
      stamp: [''],
      signature: [''],
      orders: this.fb.array([this.createOrder()]),
      invoice: [false, Validators.required],
      email: ['']
    });
  }

  // grab customers from the database and store them in a locally accessable array
  public initCustomers() {
    this.db.customerCollection.valueChanges().subscribe(data => {
      this.customers = data;
    });
  }

  // grab the products from the database and store them in a locally accessable array
  public initProducts() {
    this.db.productsCollection.valueChanges().subscribe(data => {
      this.products = data;
    });
  }

  // grab the counties from the data base and store them in a localy accessable array
  public initCounties() {
    this.db.countyCollection.valueChanges().subscribe(data => {
      this.counties = data;
    });
  }

  // initialze the truck and trailer options
  public initDropdowns() {
    this.db.truckCollection.valueChanges().subscribe(data => {
      this.trucks = data;
      console.log(data);
    });
    this.trailers = this.db.trailerCollection.valueChanges();
  }

  // select a given order[code] selected product
  public getProduct(code: string): Product {
    return this.products.find( product => {
      return product.code === code;
    });
  }
  
  // Apply transportation costs based on the type of truck used
  checkTruckType() {
    console.log('truck value', this.newTicket.get('truck').value[0]);

    const selectedTruck = this.trucks.find( truck => {
      return this.newTicket.get('truck').value[0] === truck.vehicleno;
    });

    
    if(selectedTruck.vehicleno === 'J2KT02') {
      this.addTransport('KT5', 5);
    }
    if(selectedTruck.type === 'killtruck' && selectedTruck.vehicleno != 'J2KT02') {
      this.addTransport('KT10', 5);
    }
    if (selectedTruck.type === 'truck') {
      this.addTransport('TH-1', 5);
    }
    this.addTransport('RT-T', 125);
  }

  // Add the default transportation mileage and operator costs
  addTransport(code: string, qty?: number) {
    let product = this.getProduct(code);
    this.getOrders.push(this.fb.group({
      cost: parseFloat((product.retailprice * 5).toFixed(2)),
      perUnit: product.retailprice,
      item: product.code,
      quantity: qty,
      type: product.type,
      cog: product.cog
    }));
  }

  // get method used for populating the dynamically generated form array for each item order
  get getOrders() {
    return this.newTicket.get('orders') as FormArray;
  }

  // creates the form group representing a single order
  public createOrder() {
    return this.fb.group({
      cost: 0,
      perUnit: 0,
      item: '',
      quantity: '',
      type: '',
      cog: [{value: false, disabled: true}]
    });
  }

  // method adds a single new orders to the form array orders
  public addOrder() {
    this.getOrders.push(this.createOrder());
  }

  // delete an order from the form array
  public deleteOrder(index: number) {
    this.getOrders.removeAt(index);
    if (this.getOrders.length === 0) { // if the orders array is made empty, reset the additives array
      this.resetAdditives();
    } else {
      this.updateAdditives();
    }
  }

  // Reset the additives form control to an empty array
  resetAdditives() {
    for (const item of this.getOrders.controls) {
      if (item.value.type === 'additive') {
        this.deleteOrder(this.getOrders.controls.findIndex( add => {
          return add.value.type === 'additive';
        }));
      }
    }
  }

  // check if additives are present on the ticket
  additiveExist() {
    for (const order of this.getOrders.controls) {
      if (order.value.type === 'additive') {
        return true;
      }
    }
    return false;
  }

  // initialize the additive items that will be added too the order
  public initAdditives() {
      this.getOrders.push(this.createAdditive('NE-7955'));
      this.getOrders.push(this.createAdditive('CI-60'));
      this.getOrders.push(this.createAdditive('IC-75'));
  }

  // create the inidividual form groups for each additive item
  public createAdditive(name: string) {
    const additive = this.getProduct(name);
    return this.fb.group({
      cost: 0,
      perUnit: additive.retailprice,
      item: additive.code,
      quantity: 0,
      type: additive.type,
      cog: additive.cog,
      perThousand: additive.perThousand
  });
  }

  // update the amount and cost of additives required for an order
  public updateAdditives() {
    let total = 0;
    let nefeExists = false;
    for (const order of this.getOrders.controls) {
      const product = this.getProduct(order.value.item);
      if (product) {
        if (product.type === 'nefe') {
          nefeExists = true;
          total += order.value.quantity * product.multiplier;
        }
      } else {
        console.log('No product for order item');
      }
    }

    if (nefeExists) {
      this.adjustAdditive(total);
    } else if (!nefeExists) {
      this.resetAdditives();
    }
  }

  // patch in the values for a given additives quantity and cost
  public adjustAdditive(total: number) {
    let amountOfAdditive = 0;
    let totalCost = 0;
    for (const additive of this.getOrders.controls) {
      if (additive.value.type === 'additive') {
        amountOfAdditive = ((total / 1000) * additive.value.perThousand);
        totalCost = (amountOfAdditive * additive.value.perUnit);
        additive.patchValue({
          quantity: parseFloat(amountOfAdditive.toFixed(2)),
          cost: parseFloat(totalCost.toFixed(2))
        });
      }
    }
  }

  // set the current customer and updates various fields based on customer selection
  public updateCustomerFields() {
    const selectedCustomer = this.customers.find( customer => {
      return customer.name === this.newTicket.get('customer').value;
    });
    this.newTicket.patchValue({
      ticketNo: formatDate(new Date(), 'yyMMddHHmm', 'en'), // Generate a ticketNumber based on today's date
      discount: selectedCustomer.discount,
      date: formatDate(new Date(), 'yyyy-MM-dd', 'en'), // format today's date
      seller: selectedCustomer.seller
    });
    this.newTicket.get('ticketNo').disable();
  }

  // set the currently selected county to use for tax and state
  public updateCountyFields() {
    const selectedCounty = this.counties.find( county => {
      return county.name === this.newTicket.get('county').value;
    });
    // console.log('selected county', this.selectedCounty);
    this.newTicket.patchValue({
      county: selectedCounty.name,
      tax: selectedCounty.tax,
      state: selectedCounty.state
    });
  }

  // set an order's values, enable an orders cog field, and adjust additives as necessary
  public updateItem(index: number) {
    if (this.getOrders.controls[index].get('cog').status === 'DISABLED') {
      this.getOrders.controls[index].get('cog').enable();
    }

    const product = this.getProduct(this.getOrders.controls[index].value.item);
    this.getOrders.controls[index].patchValue({
      cost: parseFloat((product.retailprice * this.getOrders.controls[index].value.quantity).toFixed(2)),
      perUnit: product.retailprice,
      type: product.type,
      cog: product.cog
    });
    if (product.type === 'nefe' && !this.additiveExist()) {
      this.initAdditives();
      this.updateAdditives();
    } else if ((product.type === 'nefe' || product.type === 'nonnefe') && this.additiveExist()) {
      this.updateAdditives();
    }
  }

  // Adjusts the total cost of an individual item
  updateItemCost(itemIndex) {
    const product = this.getProduct(this.getOrders.controls[itemIndex].value.item)
    this.getOrders.controls[itemIndex].patchValue({
      cost: parseFloat((this.getOrders.controls[itemIndex].value.perUnit * this.getOrders.controls[itemIndex].value.quantity).toFixed(2))
    });
    console.log('updating', this.getOrders.controls[itemIndex].value.item ,'ticket price');
    console.log('perUnit', this.getOrders.controls[itemIndex].value.perUnit);
    console.log('cost', this.getOrders.controls[itemIndex].value.cost);
    if (product.type === 'nefe' && !this.additiveExist()) {
      this.initAdditives();
      this.updateAdditives();
    } else if ((product.type === 'nefe' || product.type === 'nonnefe') && this.additiveExist()) {
      this.updateAdditives();
    }
  }

  // Swap all order items to their cog variants
  swapAllOrderVariants() {
    for(let i = 0; i < this.getOrders.length; i++) {
      this.getOrders.controls[i].patchValue({
        cog: !this.getOrders.controls[i].value.cog
      });
      this.swapVariant(i);
    }
  }

  // swap an individual item between COG and nonCOG variants
  swapVariant(itemIndex) {
    let code = this.getOrders.controls[itemIndex].value.item;
    if (this.getOrders.controls[itemIndex].value.cog) {
      code+=' COG';
    }
    this.db.getProductInfo(code).valueChanges().pipe(take(1)).subscribe( product => {
      if (product) {
        this.getOrders.controls[itemIndex].patchValue({
          cost: parseFloat((product.retailprice * this.getOrders.controls[itemIndex].value.quantity).toFixed(2)),
          perUnit: product.retailprice,
          type: product.type
        });
        console.log('Replacing item @' + itemIndex, 'with', product);
      } else {
        this.toastCtrl.create({
          message: `Could not find COG variant with code ${this.getOrders.controls[itemIndex].value.item + ' COG'}.`,
          duration: 2000
        }).then(toaster => {
          toaster.present();
        });
      }
    });
  }

  // Perform cog item check and submit ticket
  public onSubmit() {
    console.log(this.newTicket);
    this.newTicket.get('ticketNo').enable();
    // this.db.ticketCollection.add(this.newTicket.value);
    this.db.ticketCollection.doc(this.newTicket.get('ticketNo').value).set(this.newTicket.value);
    this.nextAction();
  }

  // Prompt user for next action.
  nextAction() {
    this.actionCtrl.create({
      header: 'Ticket Submitted. Choose next action.',
      buttons: [
        {
          text: 'Return to the Home page',
          handler: () => {
            this.navCtrl.navigateBack('/home');
            return true;
          }
        },
        {
          text: 'View the uncompleted ticket list',
          handler: () => {
            this.navCtrl.navigateForward('/viewticket');
            return true;
          }
        },
        {
          text: 'Edit the submitted ticket',
          handler: () => {
            const ticketNumber = this.newTicket.get('ticketNo').value;
            this.navCtrl.navigateForward(`/adminedit/${ticketNumber}`);
            return true;
          }
        },
        {
          text: 'Review submitted ticket details',
          handler: () => {
            const ticketNumber = this.newTicket.get('ticketNo').value;
            this.navCtrl.navigateBack(`/viewticket/view/${ticketNumber}`);
            return true;
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.initForm();
          }
        }
      ]
    }).then(navAlert => {
      navAlert.present();
    });
  }
}
