import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewticketPage } from './newticket.page';

describe('NewticketPage', () => {
  let component: NewticketPage;
  let fixture: ComponentFixture<NewticketPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewticketPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewticketPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
