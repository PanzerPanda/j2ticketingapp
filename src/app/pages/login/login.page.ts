import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(public auth: AuthenticationService, private fb: FormBuilder) { }

  oldUser: FormGroup;

  ngOnInit() {
    this.initLoginForm();
  }

  initLoginForm() {
    this.oldUser = this.fb.group({
      id: [''],
      password: ['', Validators.minLength(6)]
    });
  }

  get loginId() { return this.oldUser.get('id').value; }
  get loginPW() { return this.oldUser.get('password').value; }

  onLogin() {
    this.auth.signIn(this.loginId, this.loginPW);
  }
}
