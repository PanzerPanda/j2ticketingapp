import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmineditPage } from './adminedit.page';

describe('AdmineditPage', () => {
  let component: AdmineditPage;
  let fixture: ComponentFixture<AdmineditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmineditPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmineditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
