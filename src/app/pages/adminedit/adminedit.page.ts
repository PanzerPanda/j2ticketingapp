import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DblinkService } from 'src/app/services/dblink.service';
import { Observable } from 'rxjs';
import { Customer } from 'src/app/models/customermodel';
import { Product } from 'src/app/models/productmodel';
import { County } from '../../models/countymodel';
import { FormGroup, FormArray, FormBuilder} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NavController, ToastController, AlertController, Platform } from '@ionic/angular';
import { Order } from 'src/app/models/ticketmodel';
import { FieldRep } from 'src/app/models/fieldrepmodel';
import { Vehicle } from 'src/app/models/vehiclemodel';
import { take } from 'rxjs/internal/operators/take';

@Component({
  selector: 'app-adminedit',
  templateUrl: './adminedit.page.html',
  styleUrls: ['./adminedit.page.scss'],
})
export class AdmineditPage implements OnInit {
  @ViewChild('filePicker') filePickerRef: ElementRef<HTMLInputElement>;
  @ViewChild('container') container: ElementRef;

  customers: Customer[] = [];
  products: Product[] = [];
  counties: County[] = [];
  fieldReps: FieldRep[] = [];

  today: string;
  ticket: FormGroup;
  trucks: Vehicle[] = [];
  trailers: Vehicle[] = [];

  ticketNumber: string;

  isLoaded = false;

  usePicker: boolean;
  imageURL: string;

  fieldRepInputType = false;
  validFieldRep = true;
  originalFieldRepEmail: string;

  constructor(private db: DblinkService, private fb: FormBuilder,
              private route: ActivatedRoute, private navCtrl: NavController,
              private toastCtrl: ToastController, private alertCtrl: AlertController,
              private platform: Platform) { }

  ngOnInit() {
    this.isLoaded = false;
    this.getTicket();
    this.initCustomers();
    this.initProducts();
    this.initCounties();
    this.initDropdowns();
    if ( (this.platform.is('mobile') && !this.platform.is('hybrid')) || this.platform.is('desktop')) {
      this.usePicker = true;
    }
  }

  getTicket() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('ticketNo')) {
        this.navCtrl.navigateBack('viewticket/view');
        return;
      }
      this.db.getTicketInfo(paramMap.get('ticketNo')).valueChanges().subscribe(data => {
        this.ticket = this.fb.group({
          ticketNo: [data.ticketNo],
          afe: [data.afe],
          customer: [data.customer],
          discount: [data.discount],
          state: [data.state],
          county: [data.county],
          well: [data.well],
          date: [data.date],
          seller: [data.seller],
          truck: [data.truck],
          trailer: [data.trailer],
          tax: [data.tax],
          directions: [data.directions],
          specialdirections: [data.specialdirections],
          completed: [data.completed],
          address: [data.address],
          fieldrep: [data.fieldrep],
          stamp: [data.stamp],
          signature: [data.signature],
          orders: this.fb.array([]),
          invoice: [data.invoice],
          email: [data.email]
        });
        this.ticketNumber = data.ticketNo;
        this.initFieldReps();
        if (data.email) {
          this.originalFieldRepEmail = data.email;
        }

        this.setOrders(data.orders);
        this.isLoaded = true;
        console.log(this.ticket.value);
      });
    });
  }

  // Alert user to the number of found field reps for the customer
  public initFieldReps() {
    this.db.getCustomerFieldReps(this.ticket.get('customer').value);
    this.db.customerFieldRepCollection.valueChanges().subscribe(reps => {
      this.fieldReps = reps;
      if (this.fieldReps.length === 0) {
        this.showErrorToast(`No Field Reps available for ${this.ticket.get('customer').value}.`);
      } else {
        this.showErrorToast(`Found ${this.fieldReps.length} Field Rep(s) for ${this.ticket.get('customer').value}.`);
      }
      this.checkFieldRepInputType();
      // console.log(this.fieldReps);
    });
  }

  // grab customers from the database and store them in a locally accessable array
  public initCustomers() {
    this.db.customerCollection.valueChanges().subscribe(data => {
      this.customers = data;
    });
  }

  // grab the products from the database and store them in a locally accessable array
  public initProducts() {
    this.db.productsCollection.valueChanges().subscribe(data => {
      this.products = data;
    });
  }

  // grab the counties from the data base and store them in a localy accessable array
  public initCounties() {
    this.db.countyCollection.valueChanges().subscribe(data => {
      this.counties = data;
    });
  }

  // initialze the truck and trailer options
  public initDropdowns() {
    this.db.truckCollection.valueChanges().subscribe(data=> {
      this.trucks = data;
    });
    this.db.trailerCollection.valueChanges().subscribe(data => {
      this.trailers = data;
    });
  }

  // get method used for populating the dynamically generated form array for each item order
  get getOrders() {
    return this.ticket.get('orders') as FormArray;
  }

  // creates the form group representing a single order
  public createOrder() {
    return this.fb.group({
      cost: 0,
      perUnit: 0,
      item: '',
      quantity: '',
      type: '',
      cog: false
    });
  }

  // Apply transportation costs based on the type of truck used
  checkTruckType() {
    const selectedTruck = this.trucks.find( truck => {
      return this.ticket.get('truck').value === truck.vehicleno;
    });
    if(selectedTruck.type === 'killtruck') {
      this.addTransport('KT5', 5);
      this.addTransport('KT10', 5);
      this.addTransport('RT-T', 125);
    }
    else if (selectedTruck.type === 'truck') {
      this.addTransport('TH-1', 5);
      this.addTransport('RT-T', 125);
    }
  }

  // Add the default transportation mileage and operator costs
  addTransport(code: string, qty?: number) {
    let product = this.getProduct(code);
    this.getOrders.push(this.fb.group({
      cost: parseFloat((product.retailprice * 5).toFixed(2)),
      perUnit: product.retailprice,
      item: product.code,
      quantity: qty,
      type: product.type,
      cog: product.cog
    }));
  }

  // method adds a single new orders to the form array orders
  addOrder() {
    this.getOrders.push(this.createOrder());
  }

  // delete an order from the form array
  public deleteOrder(index: number) {
    this.getOrders.removeAt(index);
    if (this.getOrders.length === 0) { // if the orders array is made empty, reset the additives array
      this.resetAdditives();
    } else {
      this.updateAdditives();
    }
  }

  // initialize the additive items that will be added too the order
  public initAdditives() {
    this.getOrders.push(this.createAdditive('NE-7955 Non Emulsion Agent (Non-ionic)', 27.10));
    this.getOrders.push(this.createAdditive('CI-60 Corrosion Inhibitator (Double Inhibited)', 31.23));
    this.getOrders.push(this.createAdditive('IC-75 Iron Control Agent', 24.50));
  }

  // create the inidividual form groups for each additive item
  public createAdditive(name: string, puCost: number) {
    return this.fb.group({
      cost: 0,
      perUnit: puCost,
      item: name,
      quantity: 0
    });
  }

  // Reset the additives form control to an empty array
  resetAdditives() {
    for (const item of this.getOrders.controls) {
      if (item.value.type === 'additive') {
        this.deleteOrder(this.getOrders.controls.findIndex( add => {
          return add.value.type === 'additive';
        }));
      }
    }
  }

  // check if additives are present on the ticket
  additiveExist() {
    for (const order of this.getOrders.controls) {
      if (order.value.type === 'additive') {
        return true;
      }
    }
    return false;
  }

  // initialize the values of the orders form array
  setOrders(orderList: Order[]) {
    console.log(orderList);
    for (const order of orderList) {
      this.getOrders.push(this.setOrder(order));
    }
  }

  // creates the form group representing a single order
  setOrder(order) {
    console.log(order);
    if (order.type === 'additive') {
      return this.fb.group({
        item: order.item,
        quantity: order.quantity,
        perUnit: order.perUnit,
        cost: order.cost,
        type: order.type,
        cog: order.cog,
        perThousand: order.perThousand
      });
    } else {
      return this.fb.group({
        item: order.item,
        quantity: order.quantity,
        perUnit: order.perUnit,
        cost: order.cost,
        type: order.type,
        cog: order.cog
      });
    }
  }

  // update the amount and cost of additives required for an order
  public updateAdditives() {
    let total = 0;
    let nefeExists = false;
    for (const order of this.getOrders.controls) {
      const product = this.getProduct(order.value.item);
      if (product) {
        if (product.type === 'nefe') {
          nefeExists = true;
          total += order.value.quantity * product.multiplier;
        }
      } else {
        console.log('No product for order item');
      }
    }

    if (nefeExists) {
      this.adjustAdditive(total);
    } else if (!nefeExists) {
      this.resetAdditives();
    }
  }

  // patch in the values for a given additives quantity and cost
  public adjustAdditive(total: number) {
    let amountOfAdditive = 0;
    let totalCost = 0;
    for (const additive of this.getOrders.controls) {
      if (additive.value.type === 'additive') {
        amountOfAdditive = ((total / 1000) * additive.value.perThousand);
        totalCost = (amountOfAdditive * additive.value.perUnit);
        additive.patchValue({
          quantity: parseFloat(amountOfAdditive.toFixed(2)),
          cost: parseFloat(totalCost.toFixed(2))
        });
      }
    }
  }

  // set the current customer and updates various fields based on customer selection
  public updateCustomerFields() {
    const selectedCustomer = this.customers.find( customer => {
      return customer.name === this.ticket.get('customer').value;
    });
    this.ticket.patchValue({
      discount: selectedCustomer.discount,
      seller: selectedCustomer.seller
    });
    this.initFieldReps();
    this.ticket.get('ticketNo').disable();
  }

  // set the currently selected county to use for tax and state
  public updateCountyFields() {
    const selectedCounty = this.counties.find( county => {
      return county.name === this.ticket.get('county').value;
    });
    // console.log('selected county', this.selectedCounty);
    this.ticket.patchValue({
      county: selectedCounty.name,
      tax: selectedCounty.tax,
      state: selectedCounty.state
    });
  }

  // select a given order[code] selected product
  public getProduct(code: string): Product {
    return this.products.find( product => {
      return product.code === code;
    });
  }

  // set an order's values, enable an orders cog field, and adjust additives as necessary
  public updateItem(index: number) {
    if (this.getOrders.controls[index].get('cog').status === 'DISABLED') {
      this.getOrders.controls[index].get('cog').enable();
    }

    const product = this.getProduct(this.getOrders.controls[index].value.item);
    this.getOrders.controls[index].patchValue({
      cost: parseFloat((product.retailprice * this.getOrders.controls[index].value.quantity).toFixed(2)),
      perUnit: product.retailprice,
      type: product.type,
      cog: product.cog
    });
    if (product.type === 'nefe' && !this.additiveExist()) {
      this.initAdditives();
      this.updateAdditives();
    } else if ((product.type === 'nefe' || product.type === 'nonnefe') && this.additiveExist()) {
      this.updateAdditives();
    }
  }
  
  updateItemCost(itemIndex) {
    this.getOrders.controls[itemIndex].patchValue({
      cost: parseFloat((this.getOrders.controls[itemIndex].value.perUnit * this.getOrders.controls[itemIndex].value.quantity).toFixed(2))
    });
    console.log('updating', this.getOrders.controls[itemIndex].value.item ,'ticket price');
    console.log('perUnit', this.getOrders.controls[itemIndex].value.perUnit);
    console.log('cost', this.getOrders.controls[itemIndex].value.cost);
  }

  // Swap all order items to their cog variants
  swapAllOrderVariants() {
    for(let i = 0; i < this.getOrders.length; i++) {
      this.getOrders.controls[i].patchValue({
        cog: !this.getOrders.controls[i].value.cog
      });
      this.swapVariant(i);
    }
  }

  // swap an individual item between COG and nonCOG variants
  swapVariant(itemIndex) {
    let code = this.getOrders.controls[itemIndex].value.item;
    if (this.getOrders.controls[itemIndex].value.cog) {
      code+=' COG';
    }
    this.db.getProductInfo(code).valueChanges().pipe(take(1)).subscribe( product => {
      if (product) {
        this.getOrders.controls[itemIndex].patchValue({
          cost: parseFloat((product.retailprice * this.getOrders.controls[itemIndex].value.quantity).toFixed(2)),
          perUnit: product.retailprice,
          type: product.type
        });
        console.log('Replacing item @' + itemIndex, 'with', product);
      } else {
        this.toastCtrl.create({
          message: `Could not find COG variant with code ${this.getOrders.controls[itemIndex].value.item + ' COG'}.`,
          duration: 2000
        }).then(toaster => {
          toaster.present();
        });
      }
    });
  }

  // Update discount based on whole or decmial value
  updateDiscount() {
    const discount = this.ticket.get('discount').value;
    if (discount >= 1) {
      this.ticket.get('discount').setValue(discount / 100);
      return;
    }
    if (0 < discount && discount < 1) {
      this.ticket.get('discount').setValue(discount);
      return;
    }
    this.ticket.get('discount').setValue(0);
  }

  // Add a field rep email
  setSelectedFieldRepEmail() {
    const selectedFieldRep = this.fieldReps.find(fieldrep => {
      return fieldrep.name = this.ticket.get('fieldrep').value;
    });
    this.ticket.patchValue({
      email: selectedFieldRep.email
    });
    this.originalFieldRepEmail = selectedFieldRep.email;
  }

  // check if the tickets field rep already belongs to the customer on the database
  checkFieldRepInputType() {
    const ticketFieldRep = this.fieldReps.find(fieldrep => {
      return fieldrep.name === this.ticket.get('fieldrep').value;
    });
    if (ticketFieldRep) { // it does so set the fieldRepInputType to dropdown
      this.fieldRepInputType = true;
    } // else it doesn't leave it as text input
  }

  // Switches the field rep input type between dropdown and input text
  toggleFieldRep() {
    this.fieldRepInputType = !this.fieldRepInputType;
    this.checkFieldRep();
    console.log(this.fieldRepInputType);
  }

  //verify field rep name does not match customer
  checkFieldRep() {
    let fR = this.ticket.get('fieldrep').value as string;
    fR = fR.toLowerCase().trim();
    let cust = this.ticket.get('customer').value as string;
    cust = cust.toLowerCase().trim();
    console.log('fieldRep', fR);
    console.log('customer', cust);
    if (fR === cust) {
      this.showErrorToast('Field Rep name cannot match customer name.');
      this.validFieldRep = false;
    } else {
      this.validFieldRep = true;
     }
  }

  // validate a entered email address format
  validateEmail(email) {
    if (email === 'placeholder@gmail.com') {
      this.showErrorToast('placeholder@gmail.com is not a valid email address.');
      return false;
    }
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      return true;
    } else {
      this.showErrorToast(`${email} is not a valid email address`);
      return false;
    }
  }

  // add a field rep to a customers list of field reps
  addFieldRepDatabase(email) {
    if (this.validateEmail(email)) {
      console.log(`${email} is a valid email address`);
      this.db.fieldRepCollection.doc(this.ticket.get('fieldrep').value).set({
        customer: this.ticket.get('customer').value,
        email: email,
        name: this.ticket.get('fieldrep').value
      });
      this.updateTicket();
    } else {
      return true;
    }
  }

  addFieldRepTicket(email) {
    if (this.validateEmail(email)) {
      this.ticket.patchValue({
        email: email,
        name: this.ticket.get('fieldrep').value
      });
      this.updateTicket();
    } else {
      return true;
    }
  }

  // update a field rep email in the field rep collection
  updateFieldRepDatabase(frEmail){ 
    if (this.validateEmail(frEmail)) {
      console.log(`${frEmail} is a valid email address`);
      this.db.fieldRepCollection.doc(this.ticket.get('fieldrep').value).update({
        email: frEmail
      });
      this.updateTicket();
    } else {
      return true;
    }
  }

  addFieldRepAlert() {
    const frEmail: string = this.ticket.get('email').value.trim();
    this.alertCtrl.create({
      header: 'New Field Rep',
      message: `How do you want to handle:<br/>
                Name: ${this.ticket.get('fieldrep').value}<br/>
                Email: ${this.ticket.get('email').value}`,
      buttons: [
        {
          text: `Add as field rep for ${this.ticket.get('customer').value}`,
          handler: () => {
            this.addFieldRepDatabase(frEmail); // update field rep collection
          }
        },
        {
          text: `Only use for ticket ${this.ticket.get('ticketNo').value}`,
          handler: () => {
            this.addFieldRepTicket(frEmail); // only add field rep to ticket
          }
        },
        {
          text: 'Cancel ',
          role: 'cancel',
          cssClass: 'secondary'
        }
      ]
    }).then(alertPrompt => {
      alertPrompt.present();
    });
  }

  updateFieldRepAlert() {
    const frEmail: string = this.ticket.get('email').value.trim();
    this.alertCtrl.create({
      header: 'New Field Rep Email?',
      message: `How do you want to update the email for ${this.ticket.get('fieldrep').value}<br/>
                from \"${this.originalFieldRepEmail}\" to
                \"${this.ticket.get('email').value}\"? <br/>.`,
      buttons: [
        {
          text: `Update ${this.ticket.get('fieldrep').value} on database`,
          handler: () => {
            this.updateFieldRepDatabase(frEmail);
          }
        },
        {
          text: `Update ${this.ticket.get('fieldrep').value} on ticket only`,
          handler: () => {
            this.addFieldRepTicket(frEmail);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary'
        }
      ]
    }).then(alertPrompt => {
      alertPrompt.present();
    });
  }

  // Display toast messages.
  showErrorToast(data: any) {
    this.toastCtrl.create({
      message: data,
      duration: 2000
    }).then(toaster => {
      toaster.present();
    });
  }

  updateTicket() {
    this.updateDiscount();
    this.db.ticketCollection.doc(this.ticket.get('ticketNo').value).update(this.ticket.value);
    this.toastCtrl.create({
      message: `Ticket ${this.ticketNumber} updated.`,
      duration: 2000
    }).then(toaster => {
      toaster.present();
      this.navCtrl.navigateBack(`/viewticket/view/${this.ticketNumber}`);
    });
  }

  // submit the ticket to the database
  public onSubmit() {
    console.log(this.ticket);
    if (!this.fieldRepInputType && this.ticket.get('email').value && this.ticket.get('fieldrep').value) {
      this.addFieldRepAlert();
    } else if ((this.originalFieldRepEmail !== this.ticket.get('email').value.trim())
                && this.ticket.get('fieldrep').value && this.ticket.get('email').value) {
      this.updateFieldRepAlert();
    } else {
      this.updateTicket();
    }
  }
}
