import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule), canActivate: [AuthGuard] },
  { path: 'newticket', loadChildren: () => import('./pages/newticket/newticket.module').then(m => m.NewticketPageModule), canActivate: [AuthGuard] },
  { path: 'viewticket', loadChildren: () => import('./pages/viewticket/viewticket.module').then(m => m.ViewticketPageModule), canActivate: [AuthGuard] },
  { path: 'adminedit/:ticketNo', loadChildren: () => import('./pages/adminedit/adminedit.module').then(m => m.AdmineditPageModule), canActivate: [AuthGuard] },
  { path: 'newproduct', loadChildren: () => import('./pages/new/newproduct/newproduct.module').then(m => m.NewproductPageModule), canActivate: [AuthGuard] },
  { path: 'newcustomer', loadChildren: () => import('./pages/new/newcustomer/newcustomer.module').then(m => m.NewcustomerPageModule), canActivate: [AuthGuard] },
  { path: 'newcounty', loadChildren: () => import('./pages/new/newcounty/newcounty.module').then(m => m.NewcountyPageModule), canActivate: [AuthGuard] },
  { path: 'completetickets',
        loadChildren: () => import('./pages/completetickets/completetickets.module').then(m => m.CompleteticketsPageModule), canActivate: [AuthGuard] },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule) },
  { path: 'newuser', loadChildren: () => import('./pages/new/newuser/newuser.module').then(m => m.NewuserPageModule), canActivate: [AuthGuard] },  {
    path: 'newvehicle',
    loadChildren: () => import('./pages/new/newvehicle/newvehicle.module').then( m => m.NewvehiclePageModule)
  }




];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
